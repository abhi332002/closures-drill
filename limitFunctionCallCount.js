//In this code file we will use closure to solve the problem

// Define function
function limitFunctionCallCount(cb, n) {

    let count = 0;
    return function limitFunctionCall() {

        if (count < n) {
            count++;         //increase the call count
            cb();            //callback invoke
        } else {
            console.log(`!Sorry, You have attempts more than ${n} times`);
        }
    }

}

//exports function 
module.exports = { limitFunctionCallCount };