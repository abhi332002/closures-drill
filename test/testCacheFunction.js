//In this file we will test our function 

const { cacheFunction } = require("../cacheFunction");

function testCacheFunction(props){
    return props;
}

let invokeInnerFunction = cacheFunction(testCacheFunction);

invokeInnerFunction('hello');
invokeInnerFunction(10)
invokeInnerFunction(20);

//it contain all the arguments as object;
let cacheResult = invokeInnerFunction(1);
console.log(cacheResult);