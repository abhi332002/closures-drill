//In this file we will test our function 
const { limitFunctionCallCount } = require("../limitFunctionCallCount");

//define test function 
function testLimitFunctionCallCount() {
    console.log('please use your attempts carefully')
}

//call the limit function
let invokeInnerFunction = limitFunctionCallCount(testLimitFunctionCallCount, 3);

//try to invoke inner function more than the limit 
invokeInnerFunction();
invokeInnerFunction();
invokeInnerFunction();
invokeInnerFunction();