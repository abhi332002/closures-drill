//In this file we will test our function 

const { counterFactory } = require("../counterFactory");


let invokedInnerFunction = counterFactory();

//call the increment function 
invokedInnerFunction.increment();
invokedInnerFunction.increment();
invokedInnerFunction.increment();

//call the decrement function 
invokedInnerFunction.decrement();
invokedInnerFunction.decrement();
invokedInnerFunction.decrement();