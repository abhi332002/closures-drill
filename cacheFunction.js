function cacheFunction(cb) {

    const cache = {};
    return function innerCache(args) {
        cache[args] = cb(args);    //continue store the arguments `
        return cache;
    }

}

// export function
module.exports = { cacheFunction };


