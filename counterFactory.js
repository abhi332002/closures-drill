//In this code file we will use closure to solve the problem

// Define function
function counterFactory() {

    let counter = 0;
    return {
        increment() {
            counter++;                                  //increase the counter value by 1
            console.log(`after increment ${counter}`);
        },
        decrement() {
            counter--;                                  //decrease the counter value by 1
            console.log(`after decrement ${counter}`);
        }
    }
}

//export function 
module.exports = { counterFactory };


